#!/bin/bash

set -e

function print_help {
    echo "Usage:"
    echo "$0 <AWS CLI profile>"
}

if [ $# -lt 1 ]; then
    echo "Wrong count of input arguments"
    print_help
    exit 1
fi

profile=$1
bucket="peoep"

aws s3 rm s3://$bucket --recursive --profile $profile

function upload {
    for file in $1; do
        extension=${file##*.}
        content_type="text/$extension"
        if [ "$extension" = "png" ]; then
            content_type="image/png"
        fi
        aws s3api put-object --no-cli-pager --content-type $content_type --bucket $bucket --key $file --body $file --acl public-read --storage-class STANDARD --profile $profile
    done
}

upload "*.html"
upload "css/*"
upload "img/*"

